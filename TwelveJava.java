import java.util.Scanner;

public class TwelveJava {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the row; ");
		int row = scan.nextInt();

		for (int i = 1; i <= row; i++) {
			for (int j = i; j <= i+ row-1; j++) {
				if (j % 2 == 1) {
					System.out.print("1");
				} 
				else {
					System.out.print("0");
				}
				System.out.print("");
			}

			System.out.println();

		}
	}

}
