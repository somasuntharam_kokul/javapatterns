import java.util.Scanner;

public class ThirteenJava {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the row no;");
		int row = scan.nextInt();

		for (int i = 1; i <= row; i++) {
			int k = i;

			for (int j = 1; j <= i; j++) {
				System.out.print(k + " ");

				k = k + row - j;
			}

			System.out.println();
		}
	}

}
