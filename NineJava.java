import java.util.Scanner;

public class NineJava {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the row: ");
		int row = scan.nextInt();

		for (int i = 1; i <= row - 1; i++) {
			for (int k = 0; k + 1 < i; k++) {
				System.out.print(" ");
			}
			for (int j = i; j <= row; j++) {
				System.out.print(j);
			}
			System.out.println();
		}
		for (int r = 0; r < row; r++) {
			for (int z = row - 1; z > r; z--) {
				System.out.print(" ");
			}

			for (int m = row - r; m <= row; m++) {
				System.out.print(m);
			}
			System.out.println();
		}
	}

}
