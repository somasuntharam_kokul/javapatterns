import java.util.Scanner;

/*	7 
 7 6 
 7 6 5 
 7 6 5 4 
 7 6 5 4 3 
 7 6 5 4 3 2 
 7 6 5 4 3 2 1  */

import java.util.Scanner;

public class SevenJava {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the row no;");
		int row = scan.nextInt();
		for (int i = row; i >= 1; i--) {
			for (int j = 1; j <= i; j++) {
				System.out.print(j + " ");
			}
			System.out.println();
		}
	}
}
