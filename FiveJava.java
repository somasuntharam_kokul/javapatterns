import java.util.Scanner;

public class FiveJava {
	// static int num=2;
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the row no;");
		int row = scan.nextInt();
		for (int i = 0; i <= row; i++) {
			for (int j = row; j > i; j--) {
				System.out.print(j + " ");
			}
			System.out.println();
		}
	}
}
