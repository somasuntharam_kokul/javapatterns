import java.util.Scanner;

public class EightJava {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the row no;");
		int row = scan.nextInt();
		for (int i = 1; i <= row ; i++) {
			for (int j = 1; j < i; j++) {
				System.out.print(j + " ");
			}

			for (int d = i; d >= 1; d--) {
				System.out.print(d + " ");
			}
			System.out.println();

		}
	}

}
